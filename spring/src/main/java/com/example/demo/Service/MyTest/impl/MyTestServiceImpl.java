package com.example.demo.Service.MyTest.impl;

import com.example.demo.Service.MyTest.MyTestService;
import com.example.demo.dao.MyTest.MyTestDao;
import com.example.demo.pojo.MyTest.MyTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service(value = "myTestService")
public class MyTestServiceImpl implements MyTestService {
    //    自动注入DAO层数据
    @Autowired
    @Qualifier(value = "myTestDao")
    private MyTestDao myTestDao;

    //    查询所有
    @Override
    public ArrayList<MyTest> getAll() {
        return myTestDao.getAll();
    }
}
