package com.example.demo.Service.MyTest;

import com.example.demo.pojo.MyTest.MyTest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


public interface MyTestService {
    //    查询所有
    public ArrayList<MyTest> getAll();
}
