package com.example.demo.Controller.mytest;

import com.example.demo.Service.MyTest.MyTestService;
import com.example.demo.pojo.MyTest.MyTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Controller
public class MyTestController {
    //    自动注入Service层数据
    @Autowired
    @Qualifier(value = "myTestService")
    private MyTestService myTestService;

    //    导出所有数据
    @GetMapping(value = "/a")
    @ResponseBody
    public String getAll() {
        ArrayList<MyTest> all = myTestService.getAll();
        return String.valueOf(all);

    }

    @GetMapping(value = "/vip1")
    @ResponseBody
    public String vip1() {
        return "vip1";
    }

    @GetMapping(value = "/vip2")
    @ResponseBody
    public String vip2() {
        return "vip2";
    }

    @GetMapping(value = "/vip3")
    @ResponseBody
    public String vip3() {
        return "vip3";
    }

}
