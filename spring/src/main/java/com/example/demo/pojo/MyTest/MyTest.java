package com.example.demo.pojo.MyTest;

import java.io.Serializable;

public class MyTest implements Serializable {
    private String username;
    private String password;

    @Override
    public String toString() {
        return "MyTest{" +
                "userName='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public MyTest(String userName, String password) {
        this.username = userName;
        this.password = password;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MyTest() {
    }
}
