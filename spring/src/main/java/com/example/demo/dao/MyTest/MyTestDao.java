package com.example.demo.dao.MyTest;

import com.example.demo.pojo.MyTest.MyTest;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository(value = "myTestDao")
public interface MyTestDao {
//    查询所有

    @Select("select * from mytest")
    public ArrayList<MyTest> getAll();
}
