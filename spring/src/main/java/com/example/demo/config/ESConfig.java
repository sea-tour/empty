package com.example.demo.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//注册新的bean restClient

@Configuration
public class ESConfig {
    @Bean
    public RestHighLevelClient highLevelClient() {
//        返回一个新的实例 restHighLevelClient
        return new RestHighLevelClient(
//                普通的client实例
                RestClient.builder(
//                        新的HttpHost
                        new HttpHost("127.0.0.1", 9200, "http")
                )
        );
    }
}
