package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMVCConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        跳转template自定义主页
        registry.addViewController("/").setViewName("/index/index");
        registry.addViewController("/index").setViewName("/index/index");
        registry.addViewController("/index.html").setViewName("/index/index");
    }
}
