package com.example.demo.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
public class DruidConfig {
    //    注入数据源
//    放在spring容器中
    @Bean
//    注入配置文件
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource druidDataSource() {
        return new DruidDataSource();
    }

    //    自动注入spring容器新的servlet入口检测监控器
    @Bean
    public ServletRegistrationBean statViewServlet() {
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
//        设置初始化参数 封装在map集合中
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("loginUsername", "you");
        hashMap.put("loginPassword", "nnnv87");
        hashMap.put("allow", "");
        bean.setInitParameters(hashMap);
        return bean;
    }
}