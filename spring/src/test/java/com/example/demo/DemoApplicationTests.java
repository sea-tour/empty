package com.example.demo;

import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class DemoApplicationTests {

    //    注入新的Es实例
    @Autowired
    @Qualifier("highLevelClient")
    private RestHighLevelClient client;

    @Test
    void contextLoads() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = passwordEncoder.encode("nnnv87");
        System.out.println(password);
        String s = "$2a$10$ZTosHLsbq9Fa2qMKV5iTt.FvciLD4QylHgVa8z0uTAhdEmSGA8Q8u";
        String s1 = "$2a$10$HABnwmOP1HSculIX7aUf1.2NWxgs2z6mgCWqb3s197y5amtyu4pRO";
        String s2 = "$2a$10$XsKbCm0aluaId/gd5Mr5WeJNB9cxCgxfpei0tFPyvZdrI3Jv4fQDW";
        boolean b = passwordEncoder.matches("nnnv87", s);
        boolean b1 = passwordEncoder.matches("nnnv87", s1);
        boolean b2 = passwordEncoder.matches("nnnv87", s2);
        System.out.println(b + "===============" + b1 + "====================" + b2);
    }

}
