package com.example.shiro.Controller.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
//    登录页面的Controller
    @GetMapping("/login")
    public String login(){
        return "login/login";
    }

//    登录成功的返回页
    @PostMapping("/suclogin")
    public String suclogin(){
        return "redirect:index.html";
    }



}
