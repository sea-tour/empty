package com.example.shiro.Controller.shiro;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShiroController {
//    分类做出 vip1 vip2 vip3 的页面
    @GetMapping("/vip1")
    public String vip1(){
        return "vip1";
    }

    @GetMapping("/vip2")
    public String vip2(){
        return "vip2";
    }

    @GetMapping("/vip3")
    public String vip3(){
        return "vip3";
    }
}
