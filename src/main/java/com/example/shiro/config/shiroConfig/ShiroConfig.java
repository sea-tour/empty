package com.example.shiro.config.shiroConfig;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;

@Configuration
public class ShiroConfig {
//    shiro 组件 userReal 继承AuthorizingRealm 类名
    @Bean
    public MyShiroRealm userRealm(){
        return new MyShiroRealm();
    }
//    将userrealm方法继注入到DefaultWebSecurityManager
    @Bean
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm")MyShiroRealm userRealm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        return securityManager;
    }

//    自动注入securityManager 方法注入到ShiroFilterFactoryBean
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier(value = "getDefaultWebSecurityManager")DefaultWebSecurityManager securityManager ){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(securityManager);
        /*
        anon 准许所有人访问
        authc 需要认证才能访问
        user 必须拥有记住我功能才能访问
        perms 必须拥有资源的权限才能访问
        roles 必须拥有某个角色才能访问
         */
        LinkedHashMap<String, String> hashMap = new LinkedHashMap<>();
        hashMap.put("/vip1","anon");
        hashMap.put("/vip2","authc");
        hashMap.put("/vip3","roles");
        bean.setFilterChainDefinitionMap(hashMap);
        bean.setLoginUrl("/login");
        return bean;
    }
}

