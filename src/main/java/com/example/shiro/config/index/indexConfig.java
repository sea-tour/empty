package com.example.shiro.config.index;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class indexConfig implements WebMvcConfigurer {
//    重新主页跳转页面

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //主页跳转网址新位置
        registry.addViewController("/").setViewName("/index/index");
        registry.addViewController("/index").setViewName("/index/index");
        registry.addViewController("/index.html").setViewName("/index/index");
    }
}
